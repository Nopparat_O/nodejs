let http = require('http'); //build in module
let dt   = require('./myfirstmodule');
let url  = require('url');
let fs   = require('fs');  
let uc   = require('upper-case');
let rs   = fs.createReadStream('./demo.txt');
let events = require('events');
const {EventEmitter} = require('events');
let eventEmitter = new events.EventEmitter();

// -----------------------------------------------------------------------------------------------------
// Create an event handler
let myEventHandler = function() {
    console.log('I hear scream');
}
// Assign the event handler to an event
eventEmitter.on('scream', myEventHandler);

// Fire the 'scream' event
eventEmitter.emit('scream');

// -----------------------------------------------------------------------------------------------------
// Even open file
// rs.on('open', function() {
//     console.log('The file is open');
// })

// -----------------------------------------------------------------------------------------------------
// ทำ Upper case
// http.createServer(function (req, res) {
//     res.writeHead(200, {'Content-Type': 'text/html'});
//     res.write(uc.upperCase('nopparat jairat'));
//     res.end();
// }).listen(8000);

// -----------------------------------------------------------------------------------------------------
// การ Request เนื้อหาเพื่อ Response ให้ Client
// http.createServer(function (req, res) {
//     //    method parse เข้าถึง
// let q = url.parse(req.url, true);
// let filename = "." + q.pathname; // ./somepath
// fs.readFile(filename, function(err, data) {
// if (err) {
//     res.writeHead(404, {'Content-Type': 'text/html'});
//     return res.end('404 Not Found');
// }
// res.writeHead(200, { "Content-Type": 'text/html'});
// res.write(data);
// return res.end();
// })
// }).listen(8000);

// -----------------------------------------------------------------------------------------------------
// ตัด URL
// let adr = 'http://localhost:8000/default.html?year=2021&month=March';
// let q   = url.parse(adr, true);

// console.log(q.host); //returns localhost:8000
// console.log(q.pathname);
// console.log(q.search); 

// let qdata = q.query;
// console.log(qdata.year);
// -----------------------------------------------------------------------------------------------------
// http.createServer(function (req, res) {
    //ให้ response กลับมา
    // res.writeHead(200, {'Content-Type': 'text/html' });
    // res.write('The date and time are current : ' + dt.myDateTime());
    // res.write(req.url);
    // let q   = url.parse(req.url, true).query;
    // let txt = q.year + " " + q.month;
    // res.end(txt);
    // res.end('Hello World!');
// -----------------------------------------------------------------------------------------------------
    //Read file
//     fs.readFile('index.html', function(err, data) {
//         res.writeHead(200, { 'Content-Type': 'text/html'});
//         res.write(data);
//         return res.end();
//     })
// }).listen(8000); 
// -----------------------------------------------------------------------------------------------------
// Create file
// fs.appendFile('mynewfile.txt', 'Hello, World', function(err) {
//     if (err) throw err;
//     console.log('Saved!');
// })
// -----------------------------------------------------------------------------------------------------
// Create empty file for write
// fs.appendFile('mynewfile2.txt', 'w', function(err, file) {
//     if (err) throw err;
//     console.log('Saved!');
// })
// -----------------------------------------------------------------------------------------------------
// Create file เขียนทับได้
// fs.writeFile('mynewfile2.txt', 'This is replace new file 3', function(err) {
//     if (err) throw err;
//     console.log('Saved!');
// })
// -----------------------------------------------------------------------------------------------------
// Updata file
// fs.appendFile('mynewfile3.txt', 'This is update text', function(err) {
//     if (err) throw err;
//     console.log('Saved!');
// })
// -----------------------------------------------------------------------------------------------------
// Delete file
// fs.unlink('mynewfile2.txt', function(err) {
//     if (err) throw err;
//     console.log('File Deleted!');
// })
// -----------------------------------------------------------------------------------------------------
// Rename file
// fs.rename('mynewfile3.txt', 'myrenamefile.txt', function(err) {
//     if (err) throw err;
//     console.log('File Rename!');
// })
// -----------------------------------------------------------------------------------------------------